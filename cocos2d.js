/*jshint globalstrict: true*/
"use strict";

/** Tutorial de Cocos2D-HTML5, por Sebastián "ZippoLag" Vansteenkiste de Heart~Bit Games, 2013-2014 **/

(function () {
    var d = document;
    var c = {
        COCOS2D_DEBUG:2, //0: sin debug; 1: debug básico; 2: debug completo
        //CLASS_RELEASE_MODE:true, //si se define esta propiedad, puede aumentar el rendimiento del juego, pero puede generar algunas incompatibilidades con el manejo de sprites
        box2d:false, //¿Activar el motor de física box2d?
        chipmunk:false, //¿Activar el motor de física chipmunk?
        showFPS:true, //¿Mostrar contador de cuadros por segundo?
        frameRate:60, //Cuadros por segundo deseados
        loadExtension:false, //¿Cargar extensiones?
        renderMode:1,       //0: automático, 1: forzar Canvas, 2: forzar WebGL
        tag:'gameCanvas', //id del elemento <canvas> en que se dibujará el juego
        //engineDir:'../Cocos2d-html5-v2.2.1/cocos2d/', //ruta al motor de cocos2d-html5
        SingleEngineFile:'Cocos2d-html5-v2.2.1.min.js', //ruta al motor minificado
        appFiles:[
			'src/resource.js',
			'src/SceJuego.js'
        ] //archivos a cargar para nuestro juego
    };

    if(!d.createElement('canvas').getContext){ //mensaje de error en caso que el navegador no soporte canvas
        var s = d.createElement('div');
        s.innerHTML = '<h2>Your browser does not support HTML5 canvas!</h2>' +
            '<p>Google Chrome is a browser that combines a minimal design with sophisticated technology to make the web faster, safer, and easier.Click the logo to download.</p>' +
            '<a href="http://www.google.com/chrome" target="_blank"><img src="http://www.google.com/intl/zh-CN/chrome/assets/common/images/chrome_logo_2x.png" border="0"/></a>';
        var p = d.getElementById(c.tag).parentNode;
        p.style.background = 'none';
        p.style.border = 'none';
        p.insertBefore(s);

        d.body.style.background = '#ffffff';
        return;
    }
    
    window.addEventListener('DOMContentLoaded', function () { //carga los archivos del motor de cocos2d-html5

        var s = d.createElement('script');
        /*********Delete this section if you have packed all files into one*******/
        if (c.SingleEngineFile && !c.engineDir) {
            s.src = c.SingleEngineFile;
        }
        else if (c.engineDir && !c.SingleEngineFile) {
            s.src = c.engineDir + 'platform/jsloader.js';
        }
        else {
            alert('You must specify either the single engine file OR the engine directory in "cocos2d.js"');
        }
        /*********Delete this section if you have packed all files into one*******/

            //s.src = 'Packed_Release_File.js'; //aquí cargaríamos los archivos de nuestro juego si estuvieran empaquetados en un solo archivo junto al motor

        d.ccConfig = c;
        s.id = 'cocos2d-html5';
        d.body.appendChild(s);
    });
})(); //¿Ven estos "()" y el hecho de que toda la función está también entre paréntesis? Esto significa que esta función anónima se ejecutará automáticamente en cuanto se cargue
