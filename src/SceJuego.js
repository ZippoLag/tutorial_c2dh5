/*jshint globalstrict: true*/
"use strict";

//Engine:
/*global cc*/

/** Tutorial de Cocos2D-HTML5, por Sebastián "ZippoLag" Vansteenkiste de Heart~Bit Games, 2013-2014 **/

//(Clase de la) Capa principal de la Escena
var LayJuego = cc.LayerColor.extend({ //Creamos la clase de la capa principal, extendiendo (heredando) la clase LayerColor, la cual es una capa a la que se le puede definir un color de fondo
	ctor:function(){ //constructor
		this._super(); //llamada al constructor de la clase padre
		//Nota: no estamos realizando aquí acción alguna más allá de llamar al constructor de la clase padre, por lo cual en realidad no sería necesario definirlo
	},
	
	init:function(){ //función de inicialización
		//Llamamos la función de inicialización de la clase padre, que requiere un color y opcionalmente un tamaño
		this._super(cc.c4b(0,125,200,255));
		//Nota: "cc.c4b()" es una función que crea un objeto de color RGBA (rojo, verde, azul, alfa u opacidad) mediante el paso de 4 enteros valuados entre 0 y 255
		//"cc.c4b()" equivale a "new cc.Color4B()"
		
		//Creamos un objeto en el cual guardar el tamaño de la pantalla
		var tamanio = cc.Director.getInstance().getWinSize();
		//Nota: Dependiendo del editor, no debería haber problemas con utilizar caracteres como la eñe en los identificadores, pero por compatibilidad no lo haré en este momento
		
		//Creamos un objeto para ubicar el centro
		var centro = cc.p(tamanio.width/2, tamanio.height/2);
		//Nota: "cc.p()" equivale a "new cc.point()", es un atajo para crear un objeto con coordenadas (x, y)
		
		//Creamos una label para mostrar el texto de bienvenida
		var lblBienvenida = cc.LabelTTF.create("¡Bienvenido al tutorial de Cocos2D-HTML5\nde Heart~Bit Games!", "Trebuchet MS", 32);
		
		//la colocamos en el centro de la pantalla:
		lblBienvenida.setPosition(centro);
		
		//definimos que su texto se muestre alineado al centro:
		lblBienvenida.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
		
		//y la agregamos a la capa, para que sea visible
		this.addChild(lblBienvenida, 0);
		//Nota: el segundo parámetro es el "orden z", a mayor valor más elevado estará el elemento en la capa
		//Nota: aquí "this" se refiere a una instancia de esta clase LayJuego que se está inicializando, en este caso sabemos que ha sido creada por una instancia de SceJuego, pero el sistema no tiene por qué saberlo
	}
});

//(Clase de la) Escena principal del juego
var SceJuego = cc.Scene.extend({
	onEnter:function(){ //función que es llamada automáticamente cuando el control es cedido a esta escena
		this._super(); //llamamos a la función heredada del padre antes que nada
		
		//Creamos una instancia de la capa LayJuego, la inicializamos y la agregamos a la escena
		var layJuego = new LayJuego();
		layJuego.init();
		this.addChild(layJuego, 0);
		//Nota: aquí "this" se refiere a una instancia de SceJuego a la cual el Director le ha entregado el control, en este caso sabemos que ha sido creada en main.js, pero el sistema no tiene por qué saberlo
	}
});