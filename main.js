/*jshint globalstrict: true*/
"use strict";

//Engine:
/*global cc*/

//Escenas:
/*global SceJuego*/

/** Tutorial de Cocos2D-HTML5, por Sebastián "ZippoLag" Vansteenkiste de Heart~Bit Games, 2013-2014 **/

var cocos2dApp = cc.Application.extend({

    config:document.ccConfig, //objeto que guardará las configuraciones
    
    ctor:function (scene) { //carga de configuraciones definidas en cocos2d.js
        this._super();
        this.startScene = scene;

        cc.COCOS2D_DEBUG = this.config.COCOS2D_DEBUG;
        cc.initDebugSetting();

        cc.setup(this.config.tag);
        cc.AppController.shareAppController().didFinishLaunchingWithOptions();
    },
    
    applicationDidFinishLaunching:function () { //función que se llama cuando acaba el "booteo" del motor
        if(cc.RenderDoesnotSupport()){
            alert("Browser doesn't support WebGL"); //mensaje de error de que no se soporta WebGL (si se lo intentó usar)
            return false;
        }
        
        var director = cc.Director.getInstance(); //instancia del objeto Singleton Director

		//Configuración de resoluciones a utilizar:
        var screenSize = cc.EGLView.getInstance().getFrameSize();
        var resourceSize = cc.size(800, 450);
		//var resourceSize = cc.size(450, 800);
		var designSize = cc.size(800, 450);
		//var designSize = cc.size(450, 800);

		//Directorios de búsqueda de recursos
        var searchPaths = [];
        var resDirOrders = [];

        searchPaths.push("res");
        cc.FileUtils.getInstance().setSearchPaths(searchPaths);

        var platform = cc.Application.getInstance().getTargetPlatform();
        
        resDirOrders.push("HD"); //(a ser explicado)
        
		cc.FileUtils.getInstance().setSearchResolutionsOrder(resDirOrders);

		//Configuración de relación de aspecto:
        director.setContentScaleFactor(resourceSize.width / designSize.width);

		//Configuración de Políticas de redimensionamiento del canvas:
        cc.EGLView.getInstance().setDesignResolutionSize(designSize.width, designSize.height, cc.RESOLUTION_POLICY.SHOW_ALL);  //(a ser explicado)
		//Alternativas: EXACT_FIT, NO_BORDER, SHOW_ALL, FIXED_HEIGHT, FIXED_WIDTH

        director.setDisplayStats(this.config.showFPS); //¿Mostramos los cuadros por segundo?
        director.setAnimationInterval(1.0 / this.config.frameRate); //Inicializamos con los cuadros por segundo deseados

        //Realizamos la carga de recursos:
        cc.LoaderScene.preload(g_res, function () { //esta función se llama una vez terminada la carga de recursos			
			
			//Terminada la carga, invocamos nuestra escena inicial:
            director.replaceScene(new this.startScene());
        }, this);

        return true;
    }
});

//Configuración de la escena inicial del juego:
var myApp = new cocos2dApp(SceJuego);